__author__ = 'abdulhaseeb'

from itertools import izip_longest


class Polynomial:

	def __init__(self, coefficients = [0.0]):

		self.coeffs = [i for i in coefficients]

	def evalAt(self, x):
		sum = 0;
		for power, coeff in enumerate(self.coeffs):
			sum += coeff * pow(x, power)
		return sum

	def __add__(self, poly):
		return self.__class__([a+b for a,b in izip_longest(self.coeffs, poly.coeffs, fillvalue=0)])

	def __sub__(self, poly):
		return self.__class__([a-b for a,b in izip_longest(self.coeffs, poly.coeffs, fillvalue=0)])

	def __mul__(self, arg):
		if isinstance(arg, Polynomial):
			newcoeffs = [0] * (len(self.coeffs) + len(arg.coeffs) - 1)
			for mypower, mycoeff in enumerate(self.coeffs):
				for argpower, argcoeff in enumerate(arg.coeffs):
					newcoeffs[argpower + mypower] += mycoeff * argcoeff
		else:
			newcoeffs = [arg*x for x in self.coeffs]

		return self.__class__(newcoeffs)

	def __str__(self):
		strings = []
		for pow, coeff in enumerate(self.coeffs):
			if coeff != 0:
				if pow == 0:
					s = ''
				elif pow == 1:
					s = 'X'
				else:
					s = 'X^' + str(pow)
				strings.append(str(coeff) + ' ' + s)
		if len(strings) > 0:
			strings.reverse()
			return ' + '.join(strings)
		else:
			return '0'

def polytest():
	p1 = Polynomial([1, 2])
	p2 = Polynomial([1, 2])

	addition = p1 + p2
	print addition

	subt = p1 - p2
	print subt

	print str(p1.evalAt(10))

	mult = p1 * p2

	print mult