__author__ = 'abdulhaseeb'

#from Polynomial import Polynomial


def fulldomain_interpolate(x, y):
	poly = Polynomial([0])
	for i in range(len(x)):

		num = Polynomial([1])
		den = 1

		for j in range(len(y)):
			if (i != j):
				den *= x[i] - x[j]
				num *= Polynomial([-1*x[j], 1])

		#print str(x)
		poly += num * (y[i]/den)
		#print poly

	return poly

def q1a():
	b = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
	h = [0.0, 14.7, 36.5, 71.7, 121.4, 197.4]

	lg = fulldomain_interpolate(b, h)
	print 'P(x) = ', lg
	print lg.evalAt(0.0)
	print lg.evalAt(0.2)
	print lg.evalAt(0.4)
	print lg.evalAt(0.6)
	print lg.evalAt(0.8)
	print lg.evalAt(1.0)
	return

def q1b():
	b = [0.0, 1.3, 1.4, 1.7, 1.8, 1.9]
	h = [0.0, 540.6, 1062.8, 8687.4, 13924.3, 22650.2]

	lg = fulldomain_interpolate(b, h)
	print 'P(x) = ', lg
	print lg.evalAt(0.0)
	print lg.evalAt(1.3)
	print lg.evalAt(1.4)
	print lg.evalAt(1.7)
	print lg.evalAt(1.8)
	print lg.evalAt(1.9)
	return