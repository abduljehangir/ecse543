__author__ = 'Abdul Haseeb Jehangir'

import io_lib
from random import randrange
from math import sqrt


class Matrix:

    def __init__(self, rows, columns = None):
        if type(rows) is list:
            if type(rows[0]) is list:
                self.matrix = rows
                self.size = [len(rows), len(rows[0])]
            else:
                self.matrix = [rows]
                self.size = [1, len(rows)]
        elif None == columns:
            self.matrix = [[0 for x in range(rows)] for y in range(rows)]
            self.size = [rows, rows]
        else:
            self.matrix = [[0 for x in range(columns)] for y in range(rows)]
            self.size = [rows, columns]

    @property
    def size(self):
	    return self.size

    @property
    def property(self):
	    return self.matrix

    def multiply(self, b):
        if not self.size[1] == b.size[0]:
            raise AssertionError("Matrix size not compatible for multiplication")

        product = Matrix(self.size[0], b.size[1])

        for i in range(self.size[0]):   # traverse through row of first matrix
            for j in range(b.size[1]):  # traverse through column of second matrix
                sum = 0
                for k in range(b.size[0]):   # traverse through rows for second matrix
                    sum += self.matrix[i][k] * b.matrix[k][j]
                product.matrix[i][j] = sum
        return product

    def transpose(self):
        '''
        :return: Transpose of the provided matrix
        '''
        t = Matrix(self.size[1], self.size[0])
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                t.matrix[j][i] = self.matrix[i][j]
        return t

    def pd(self):
        """
        :return: True if matrix is positive definite. False if otherwise
        """
        # CONDITION: x'Ax > 0

        #define a non-zero x
        pd = False
        x = Matrix(range(1, self.size[0]+1))
        x_t = Matrix.transpose(x)


        result = Matrix.multiply(x.multiply(self), x_t)

        if result.matrix[0][0] > 0:
            pd = True
        return pd

    def symmetric(self):
        return self.matrix == Matrix.transpose(self).matrix

    def add(self, b, subtract = None):
        if not ((self.size[1] == b.size[1]) and (self.size[0] == b.size[0])):
            raise AssertionError("Matrix size not compatible for addition/subtraction")

        s = Matrix(self.size[0], self.size[1])
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                if None == subtract:
                    s.matrix[i][j] = self.matrix[i][j] + b.matrix[i][j]
                else:
                    s.matrix[i][j] = self.matrix[i][j] - b.matrix[i][j]
        return s

    def subtract(self, b):
        return Matrix.add(self, b, 1)

    def scaler_mul(self, Num):
        """
        :param Num: Scaler multiple
        :return: Matrix multiplied with scaler number Num
        """
        scaled = Matrix(self.size[0], self.size[1])
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                scaled.matrix[i][j] = Num * self.matrix[i][j]
        return scaled

    def chol(self, b = None, bw = None):
        """
        :return: returns lower triangular cholesky decomposed matrix
        """
        if not self.pd():
            raise AssertionError("Matrix is not positive defininte")
        if not(cmp(self.matrix, Matrix.symmetric(self)) != 0):
            raise AssertionError("Matrix is not symetric")
        L = self
        # process column by column

        for j in range(self.size[1]):   # traverse through columns

            if bw != None:
	            row_range = min(j + bw, self.size[0])
            else:
	            row_range = self.size[0]
            for i in range(0, row_range):   # traverse through rows
                if j > i:
                    L.matrix[i][j] = 0
                elif i == j:
                    L.matrix[i][j] = sqrt(L.matrix[i][j])
                    if None != b:
                        b.matrix[j][0] /= L.matrix[i][j]
                else:
                    L.matrix[i][j] /= L.matrix[j][j]
                    if None != b:
                        b.matrix[i][0] -= L.matrix[i][j] * b.matrix[j][0]
                    for k in range(j+1, i+1):
                        # Look ahead method
                        L.matrix[i][k] -= L.matrix[i][j] * L.matrix[k][j]
        if None == b:
            return L
        else:
            return [L, b]

    def solve_eq(self, b, bw = None):
        '''
        This function solves a system of linear equations. Assumes A is a real,
        symmetric, positive definite matrix of order n.
        :param b: b matrix in eq Ax=b
        :return: returns nx1 matrix x in eq Ax=b
        '''
        if not (type(b.matrix) is list):
            raise AssertionError("B should be a nx1 matrix")
        elif b.size[0] != self.size[0]:
            raise AssertionError("B should be a nx1 matrix")

        [L,y] = self.chol(b, bw)
        L_t = Matrix.transpose(L)

        for i in reversed(range(b.size[0])):
            y.matrix[i][0] /= L.matrix[i][i]
            #look ahead
            for j in range(i):
                y.matrix[j][0] = y.matrix[j][0] - L.matrix[i][j] * y.matrix[i][0]
        return y

    def __str__(self):
	    chars = io_lib.print_list(self.matrix[0])
	    for i in range(1, len(self.matrix)):
		    chars = chars + ";" + io_lib.print_list(self.matrix[i])
	    return chars

    @staticmethod
    def eye(n):
        I = Matrix(n)
        for i in range(n):
            I.matrix[i][i] = 1
        return I

    @staticmethod
    def gen_sym_pd(n):
        a = Matrix(n)
        for i in range(n):
            for j in range(i):
                a.matrix[i][j] = randrange(1, 10)

        return a.multiply(a.transpose())

    @staticmethod
    def matcpy(dst, src):

	    dst.size[0] = src.size[0]
	    dst.size[1] = src.size[1]

	    for i in range(dst.size[0]):
		    for j in range(src.size[1]):
			    dst.matrix[i][j] = src.matrix[i][j]
	    return dst