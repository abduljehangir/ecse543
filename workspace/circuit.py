__author__ = 'abdulhaseeb'

from mat_lib import Matrix
from math import pow
from math import sqrt
import io_lib


def circuit_solver(f, fast = False):
	lines = io_lib.read_circuit_file(f)

	j = Matrix(lines[0]).transpose()
	r = Matrix(lines[1])
	e = Matrix(lines[2]).transpose()
	i = Matrix(lines[3])

	# transform J into Y
	# Y is a square matrix

	y = Matrix(r.size[1])

	for index in range(y.size[0]):
		y.matrix[index][index] = 1 / r.matrix[0][index]

	a = i.multiply(y).multiply(i.transpose())
	b = i.multiply(j.subtract(y.multiply(e)))

	if fast == True:
		bw = int(sqrt(i.size[0] + 1)+ 1)
	else:
		bw = None

	v = a.solve_eq(b, bw)
	return v


def circuit_grid(n):
	# number of branches equal to 2n^2-2n + 1
	# total number of nodes: m * n
	# branch resistance is 1 kOhm
	if n < 2:
		raise AssertionError("N should be an integer greater than 1")
		return -1

	cols = int(2*pow(n,2) - 2*n + 1)
	rows = int(pow(n,2))

	I = Matrix(rows, cols)
	for i in range(n):
		for j in range(n):
			node = n*i + j
			left_branch = (n-1)*i + (n*i) + j - 1
			right_branch = left_branch + 1
			top_branch = (i * (n-1)) + ((i-1)*n) + j
			bottom_branch = top_branch + 2*n - 1

			if i == n-1 and j == 0:
				continue
			if i > 0:
				I.matrix[node][top_branch] = 1
			if i < (n-1):
				I.matrix[node][bottom_branch] = -1
			if j > 0:
				I.matrix[node][left_branch] = -1
			if j < (n-1):
				I.matrix[node][right_branch] = 1

	R = [1000] * cols
	J = [0] * cols
	E = [0] * cols
	E[cols - 1] = -1
	#add current path in I for external resistance
	I.matrix[n-1][cols - 1] = 1
	#remove the ground node on lower left corner. node is at i = n-1, j = 0
	ground_row = I.matrix[n*(n-1)]
	I.matrix.remove(ground_row)

	file_name = "grid_" + str(n) + ".txt"
	io_lib.create_circuit_file(Matrix(J), Matrix(R), Matrix(E), I, file_name)
	return file_name

def mesh_resistance(n, fast = False):
	if n < 2:
		raise AssertionError("N should be an integer greater than 1")
		return -1
	file_name = circuit_grid(n)
	if file_name != -1:
		v = circuit_solver(file_name, fast)
		v_1 = 1
		v_2 = v.matrix[n-1][0]
		r_1 = 1000
		#voltage dividor
		resistance = (r_1 * v_2) / (v_1 - v_2)
		return resistance
	return -1