__author__ = 'abdulhaseeb'

from mat_lib import Matrix
from math import sqrt
import io_lib


def read_a_b_file(filename):
	lines = io_lib.read_circuit_file(filename)
	A = Matrix(lines[0])
	b = Matrix(lines[1])

	return [A, b]


def use_cholesky():
	[A, b] = read_a_b_file("q3.csv")
	#print A
	b = (A.transpose()).multiply(b)
	A = (A.transpose()).multiply(A)
	x = A.solve_eq(b)
	return x

def use_cg():
	[A, b] = read_a_b_file("q3.csv")

	# make sure A is positive definite
	b = (A.transpose()).multiply(b)
	A = (A.transpose()).multiply(A)

	#Initialize x, r, p

	#solution vector x. Initialized to zero
	x = Matrix(b.size[0], 1)
	#direction vector
	r = b.subtract(A.multiply(x))
	p = Matrix(b.size[0], 1)
	Matrix.matcpy(p, r)

	norm_2_list = [norm_2(r)]
	norm_inf_list = [norm_inf(r)]


	for k in range(b.size[0]):
		p_t = p.transpose()
		alpha = (p_t.multiply(r)).matrix[0][0] / (p_t.multiply(A.multiply(p))).matrix[0][0]
		x = x.add(p.scaler_mul(alpha))
		r = b.subtract(A.multiply(x))

		# norm per iteration
		norm_2_list.append(norm_2(r))
		norm_inf_list.append(norm_inf(r))

		beta = -1 * (p_t.multiply(A.multiply(r))).matrix[0][0] / (p_t.multiply(A.multiply(p))).matrix[0][0]
		p = r.add(p.scaler_mul(beta))

	print norm_2_list
	print norm_inf_list

	return x


def norm_2(x):
	s = 0
	for i in range(x.size[0]):
		s += x.matrix[i][0] * x.matrix[i][0]
	return sqrt(s)


#assumes that x is a nx1 Matrix
def norm_inf(x):
	return max(max(x.matrix))