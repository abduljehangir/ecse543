__author__ = 'abdulhaseeb'


def read_circuit_file(filename):
	reader = open(filename)
	lines = reader.readlines()

	for i in range(len(lines)):
		lines[i] = lines[i].split(";")
		for j in range(len(lines[i])):
			lines[i][j] = lines[i][j].split(",")
			for k in range(len(lines[i][j])):
				lines[i][j][k] = float(lines[i][j][k])

	return lines

def create_circuit_file(J, R, E, A, file_name):
	f = open(file_name, 'w')
	chars = str(J) + "\n" + str(R) + "\n" + str(E) + "\n" + str(A)
	f.write(chars)
	f.close()

def print_list(l):
	chars = str(l)
	chars = chars.strip('[')
	chars = chars.strip(']')
	return chars