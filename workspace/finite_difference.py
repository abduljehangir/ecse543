from __future__ import division
__author__ = 'abdulhaseeb'

from mat_lib import Matrix


def sor_fixed_mesh(w, h):
	# Calculate the number of nodes depending on the value of h
	# We know the dimentions of our figure.
	# Using symmetry to break the problem into 1/4th of the figure.
	n_x = int(0.1/h + 1)
	n_y = n_x

	#calculating number of nodes lying in the 10V block in x and y direction
	fixed_x = int(0.04/h)
	fixed_y = n_y - int(0.02/h) - 1

	# Identifier to tell when to stop iterations
	stop = False;
	iterations = 0;

	#create matrix. Each node represented by a node on this matrix.
	v = Matrix(n_x, n_y)

	#set 10V on nodes inside the 10V block. Including the nodes on the boundary
	for j in range(fixed_x + 1):
		for i in range(fixed_y, n_y):
			v.matrix[i][j] = 10

	# devide the upper right quarter into 4 regions. Let:
	# Region 1: neumann boundary along y axis
	# Region 2: neumann boundary along x axis
	# Region 3: 1.04 < x < 0.2, 0.1 < y < 1.02
	# Region 4: 0.1 < x < 0.2, 0.12 < y < 0.2

	while (stop == False):
		iterations += 1

		for j in range (n_x):
			for i in reversed(range(n_y)):
				r_1 = (j == 0) and (i < fixed_y) and i > 0
				if r_1:
					v.matrix[i][j] = (1-w) * v.matrix[i][0] + (v.matrix[i - 1][0] + v.matrix[i + 1][0] + 2*v.matrix[i][1]) *w/4
				r_2 = (i == n_y - 1) and (j < n_x - 1) and (j > fixed_x)
				if r_2:
					v.matrix[i][j] = (1-w) * v.matrix[i][j] + (v.matrix[i][j+1] + v.matrix[i][j-1] + 2 * v.matrix[i-1][j]) * w / 4
				r_3 = (j > fixed_x) and (j < n_x - 1) and (i >= fixed_y) and (i < n_y - 1)
				r_4 = (j > 0) and (j < n_x - 1) and (i > 0) and (i < fixed_y)
				if (r_3 or r_4):
					v.matrix[i][j] = (1-w) * v.matrix[i][j] + (v.matrix[i][j-1] + v.matrix[i][j+1] + v.matrix[i-1][j] + v.matrix[i+1][j]) * w / 4

		stop = residue(v, h)

	return [v, iterations]



def residue(v, h):
	# we canculate residue at each node. If residue meets our requirements at all
	# nodes, we return true, else we return false.
	const = 0.00001
	n_x = int(0.1/h + 1)
	n_y = n_x

	#calculating number of nodes lying in the 10V block in x and y direction
	fixed_x = int(0.04/h)
	fixed_y = n_y - int(0.02/h) - 1

	for j in range(n_x):
		for i in reversed(range(n_y)):
			r_1 = (j == 0) and (i < fixed_y) and i > 0
			if r_1:
				res = abs(v.matrix[i - 1][j] + v.matrix[i + 1][j] + 2*v.matrix[i][j+1] - 4 * v.matrix[i][j])
				if res >= const:
					return False
			r_2 = (i == n_y - 1) and (j < n_x - 1) and (j > fixed_x)
			if r_2:
				res = abs(v.matrix[i][j+1] + v.matrix[i][j-1] + 2 * v.matrix[i-1][j] - 4 * v.matrix[i][j])
				if res >= const:
					return False
			r_3 = (j > fixed_x) and (j < n_x - 1) and (i >= fixed_y) and (i < n_y - 1)
			r_4 = (j > 0) and (j < n_x - 1) and (i > 0) and (i < fixed_y)
			if (r_3 or r_4):
				res = abs(v.matrix[i][j+1] + v.matrix[i][j-1] + v.matrix[i-1][j] + v.matrix[i+1][j] - 4 * v.matrix[i][j])
				if res >= const:
					return False

	return True

def explore_w_sos():
	w = 1
	incr = 0.1
	for i in range(10):
		[v, itrs] = sor_fixed_mesh(w, 0.02)

		print "w: ", w, " iterations: ", itrs, "potential(0.06, 0.04): ", v.matrix[2][2]
		w += incr

def explore_h_sos():
	w = 1.3
	h = [1/50, 1/100, 1/200, 1/300, 1/400, 1/500, 1/1000]
	for i in h:
		[v, itrs] = sor_fixed_mesh(w, i)
		print "h: ", i, " iterations: ", itrs, "potential(0.06, 0.04): ", v.matrix[int(0.1/i-0.06/i)][int(0.04/i)]

def jacobi_fixed_mesh(h):
	# Calculate the number of nodes depending on the value of h
	# We know the dimentions of our figure.
	# Using symmetry to break the problem into 1/4th of the figure.
	n_x = int(0.1/h + 1)
	n_y = n_x

	#calculating number of nodes lying in the 10V block in x and y direction
	fixed_x = int(0.04/h)
	fixed_y = n_y - int(0.02/h) - 1

	# Identifier to tell when to stop iterations
	stop = False;
	iterations = 0;

	#create matrix. Each node represented by a node on this matrix.
	v = Matrix(n_x, n_y)
	v_1 = Matrix(n_x, n_y)
	#set 10V on nodes inside the 10V block. Including the nodes on the boundary
	for j in range(fixed_x + 1):
		for i in range(fixed_y, n_y):
			v.matrix[i][j] = 10
			v_1.matrix[i][j] = 10

	while (stop == False):
		iterations += 1

		for j in range (n_x):
			for i in reversed(range(n_y)):
				r_1 = (j == 0) and (i < fixed_y) and i > 0
				if r_1:
					v_1.matrix[i][j] = (v.matrix[i - 1][0] + v.matrix[i + 1][0] + 2*v.matrix[i][1]) * 0.25
				r_2 = (i == n_y - 1) and (j < n_x - 1) and (j > fixed_x)
				if r_2:
					v_1.matrix[i][j] = (v.matrix[i][j+1] + v.matrix[i][j-1] + 2 * v.matrix[i-1][j]) * 0.25
				r_3 = (j > fixed_x) and (j < n_x - 1) and (i >= fixed_y) and (i < n_y - 1)
				r_4 = (j > 0) and (j < n_x - 1) and (i > 0) and (i < fixed_y)
				if (r_3 or r_4):
					v_1.matrix[i][j] = (v.matrix[i][j-1] + v.matrix[i][j+1] + v.matrix[i-1][j] + v.matrix[i+1][j]) * 0.25

		stop = residue(v_1, h)
		#copy contents of v_1 into v
		Matrix.matcpy(v, v_1)


	return [v, iterations]

def explore_h_jacobi():
	h = [1/50, 1/100, 1/200, 1/300, 1/400, 1/500, 1/1000]
	for i in h:
		[v, itrs] = jacobi_fixed_mesh(i)
		#print "h: ", i, " iterations: ", itrs, "potential(0.06, 0.04): ", v.matrix[int(0.1/i-0.06/i)][int(0.04/i)]
		print "h:, ", i, "iterations:, ", itrs, "potential matrix: ", v