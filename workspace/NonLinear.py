__author__ = 'abdulhaseeb'

from math import log
from math import exp

def NewtonRaphson(b, h):
	psi = 0
	err = 1
	iters = 0

	flux = [i/10000 for i in b]

	while (err > 0.000001):
		[i, k] = threshold_psi(psi, flux)
		h_der = (h[i + 1] - h[i]) / (flux[i + 1] - flux[i])
		if k == 0:
			h_val = h_der * (psi - flux[i]) + h[i]
		else:
			h_val = h[i]
		f_w = 0.3 * h_val + 3.9789e7 * psi - 8000
		f_der = 0.3 * h_der + 3.9789e7
		#next guess
		psi -= f_w/f_der
		if psi < 0:
			psi = 0
		err = abs(f_w / 8000)

		iters += 1

	return [psi, iters]

def SuccessiveSubstitution(b, h, f_der):
	psi = 0
	err = 1
	iters = 0
	flux = [i/10000 for i in b]

	while (err > 0.000001):
		[i, k] = threshold_psi(psi, flux)
		h_der = (h[i + 1] - h[i]) / (flux[i + 1] - flux[i])
		if k == 0:
			h_val = h_der * (psi - flux[i]) + h[i]
		else:
			h_val = h[i]
		f_w = 0.3 * h_val + 3.9789e7 * psi - 8000
		#f_der = 0.3 * h_der + 3.9789e7
		#next guess
		psi -= f_w/f_der
		if psi < 0:
			psi = 0
		err = abs(f_w / 8000)

		iters += 1
		if (iters > 5000):
			print "unable to converge"
			break

	return [psi, iters]

def threshold_psi(psi, flux):
	for i in range(len(flux) - 1):
		f1 = flux[i]
		f2 = flux[i+1]
		if f1 < psi and psi < f2:
			return [i, 0]
		elif f1 == psi:
			return [i, 1]
		elif f2 == psi:
			return [i+1, 1]
	return [i, 1]

def solve_diode_circuit(e, r, isa, isb):
	vt = 25e-3
	ratio = isa/isb
	min_err = 1e-10

	#initial guess
	v1 = 0

	err = 1
	iters = 0
	#vectors to update
	va_step = []
	vb_step = []
	f_step = []


	while(err > min_err):
		f_v = v1 + vt * log(ratio*exp(v1/vt) - ratio + 1) - e + isa * r * (exp(v1/vt) - 1)
		f_der = 1 + (exp(v1/vt))/(exp(v1/vt) - 1 + ratio) + isa * r / vt * exp(v1/vt)

		v1 = v1 - f_v/ f_der
		v2 = vt * log(ratio*exp(v1/vt) - ratio + 1)
		iters += 1
		err = abs(f_v)
		va_step.append(v1)
		vb_step.append(v2)
		f_step.append(f_v)

	return [va_step, vb_step, f_step, iters]

b = [0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9]
h = [0, 14.7, 36.5, 71.7, 121.4, 197.4, 256.2, 348.7, 540.6, 1062.8, 2318.0, 4781.0, 8687.4, 13924.3, 22650.2]
[flux, iterations] = NewtonRaphson(b, h)
print 'flux: ', flux
print 'iterations: ', iterations

[flux, iterations] = SuccessiveSubstitution(b, h, 1e8)
print 'flux: ', flux
print 'iterations: ', iterations
x = []
[va_step, vb_step, f_step, iters] = solve_diode_circuit(200e-3, 500, 0.8e-6, 1.1e-6)
x = [abs(0.0951 - i) for i in va_step]
print va_step
print vb_step
print f_step
print iters
print x