from __future__ import division
__author__ = 'abdulhaseeb'

from math import sin
from math import log


def integrate(x0, x1, n, function):

	#make intervals in x
	if (n == 1):
		x = [n]
	else:
		step = (x1 - x0) / (n - 1)
		x = [x0 + step * i for i in range(n)]

	sum = 0

	for i in range(n - 1):
		if function == 1:
			sum += (x[i+1] - x[i]) * sin((x[i+1] + x[i]) / 2)
		elif function == 2:
			sum += (x[i+1] - x[i]) * log((x[i+1] + x[i]) / 2)
		elif function == 3:
			sum += (x[i+1] - x[i]) * log(0.2 * abs(sin((x[i+1] + x[i]) / 2)))

	return sum

def q3a():
	correct_answer = 0.45970
	err_vec = []
	for n in range(1, 21):
		area = integrate(0, 1, n, 1)
		err_vec.append(abs(correct_answer - area))
	print err_vec
	print area


def q3b():
	correct_answer = -1
	err_vec = []
	for n in range(1, 21):
		area = integrate(0, 1, n*10, 2)
		err_vec.append(abs(correct_answer - area))
	print err_vec
	print area

def q3c():
	correct_answer = -2.66616
	err_vec = []
	for n in range(1, 21):
		area = integrate(0, 1, n*10, 3)
		err_vec.append(abs(correct_answer - area))
	print err_vec
	print area

def integrate_uneven(x0, x1, function, window_vector):

	#make 10 intervals in x
	s = sum(window_vector)
	windows = []
	for i in window_vector:
		size = i * (x1 - x0) / s
		windows.append(size)

	s = 0
	runningsum = 0

	for window in windows:
		if function == 1:
			s += window * sin((window + 2*runningsum) / 2)
		elif function == 2:
			s += window * log((window + 2*runningsum) / 2)
		elif function == 3:
			s += window * log(0.2 * abs(sin((window + 2*runningsum) / 2)))
		runningsum += window
	return s

def q3d():
	correct_answer = -1
	err_vec = []
	area = integrate_uneven(0, 1, 2, [1,2,4,8,16,32,64,128,256,512, 1024])
	err_vec.append(abs(correct_answer - area))
	print err_vec
	print area
	correct_answer = -2.66616
	err_vec = []
	area = integrate_uneven(0, 1, 3, [1,2,4,8,16,32,64,128,256, 512])
	err_vec.append(abs(correct_answer - area))
	print err_vec
	print area